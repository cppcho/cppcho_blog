Add new content

```
hugo new posts/my-first-post.md
```

Start server
```
hugo server -D
# -D build draft
```

Build static pages
```
hugo -D
```
