+++
title = "Python SimpleHTTPServer"
date = 2020-04-19T15:03:24+09:00
draft = true
categories = ["dev"]
tags = ["python"]
+++

Open file server in current directory.

{{< highlight bash >}}
python -m SimpleHTTPServer 3333
{{< / highlight >}}

Access in browser.
{{< highlight bash >}}
<ip-of-server>:3333
{{< / highlight >}}

